"""
Copyright 2021 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
import nord.nord.Receiver as Receiver
import base64


class ReqBase(Receiver):

    def basic_auth_token(self, rule):
        """
        This is called by the edge.apply_delegate via object interrogation.

        In doing so, the argument is now named, and will be provided to the
        request.xxx method as the named keyword argument
        """
        val = rule.edge.get_cargo()
        # get the argument, set its named value
        arg = self.get_argument(rule)
        arg.set_named_payload('headers', {'Authorization': 'Basic ' + base64.b64encode(val.encode()).decode('UTF-8')})

    def execute(self, runtime):
        """
        Call the descendent's method and store the returned
        response in value
        """
        if len(self.get_sources('pass')) == 0:
            pass
        else:
            args, kwargs = self.get_arg_values()
            print(str(args) + str(kwargs))
            self._value = self._method(*args, **kwargs)
        self.flag_exe(runtime)

    def get(self):
        if hasattr(self, '_value'):
            return self._value
        else:
            return None
