"""
Copyright 2021 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
import nord.nord.InstanceOf as InstanceOf
import sys


class Response(InstanceOf):
    def set(self, value):
        self._response = value

    def produce_status(self, rule):
        """load the response status code as the edge cargo"""
        rule.edge.set_cargo(self._response.status_code)

    def produce_byte_content(self, rule):
        """load the response byte content as the edge cargo"""
        rule.edge.set_cargo(self._response.content)

    def produce_text_content(self, rule):
        """load the response text content as the edge cargo"""
        rule.edge.set_cargo(self._response.text)

    def produce_json_content(self, rule):
        """load the response text content as the edge cargo"""
        rule.edge.set_cargo(self._response.json())


sys.modules[__name__] = Response
