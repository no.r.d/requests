"""
Copyright 2021 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
from nord.sigurd.shell.visnode import VisNode
from nord.sigurd.utils.verboser import funlog
import nord.sigurd.managers.statemanager as sm


class Response(VisNode):
    """Implement the visualization."""

    def __init__(self, uid, app):
        """Wrap the parent class constructor."""
        super().__init__(uid, 'response', app, space="requests")

        self.add_delegate("produce_status", "responsestatus", 'pass', 'as_src',
                          tiptext=_("status code passed\nfrom here"))  # noqa: F821
        self.add_delegate("produce_byte_content", "responsebytes", 'assign', 'as_src',
                          tiptext=_("response bytes passed\nfrom here"))  # noqa: F821
        self.add_delegate("produce_text_content", "responsetext", 'assign', 'as_src',
                          tiptext=_("response text passed\nfrom here"))  # noqa: F821
        self.add_delegate("produce_json_content", "responsejson", 'assign', 'as_src',
                          tiptext=_("response JSON passed\nfrom here"))  # noqa: F821

    def process_delegated_click(self, mapentry, payload):
        """
        Placeholder for descendents to override.
        """
        dec = mapentry
        rule = {'responsestatus': 'pass',
                'responsebytes': 'assign',
                'responsetext': 'assign',
                'responsejson': 'assign'}.get(dec.name, None)
        if rule is None:
            raise Exception("IMPLEMENTATION ERROR")
        sm.model_start_connection(dec, rule)

    def connect_allowed(self, other, ctype, direction, delegate=None):
        """
        Do not allow connections of type pass to attach to this node directly.

        Only delegates can be passed to. However, delegates are resolved
        after graph/map load. So, in order to prevent, live-edit time
        connections, only check when called by the delegate (which means)
        that delegate will not be none.
        """
        if direction == 'as_src' and delegate is not None:
            if ctype == 'pass':
                if delegate.uid.split(':')[0] in ('produce_status'):
                    return True
                else:
                    return False
            elif ctype == 'assign':
                if delegate.uid.split(':')[0] in (
                                    'produce_byte_content',
                                    'produce_text_content',
                                    'produce_json_content'):
                    return True
                else:
                    return False
            else:
                return False
        else:
            return True

    @funlog
    def update_tooltip(self, tooltip):
        if hasattr(self, 'mapentry') and hasattr(self.mapentry, '_response'):
            tooltip.set_text(f"{self._response.status_code}: {self._response.text} ")
            return
        tooltip.set_text(f"{self.name}: " + _("** Unset **"))  # noqa: F821
