{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ]
            }
        },
        "enrique": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://requests/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/requests/tests/maps",
                    "dir://tests/maps"
                ],
                "path": [
                    "dir://requests/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/requests/tests/maps",
                    "dir://tests/maps"
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ]
            }
        },
        "jabir": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ]
            }
        },
        "kamal": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "requests": {
            "locations": {
                "code": [
                    "null://.",
                    "dir:///Users/nate/Desktop/Dev/NoRD/requests/src/nord/requests/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/requests/src/nord"
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/requests/src/nord/requests",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/requests/src/nord/requests/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/requests/src/nord"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/requests/src/nord/requests",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/requests/src/nord/requests/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/requests/src/nord"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        },
        "xuyue": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "1ae4bea6-b333-47fb-a15a-f0189eeca9af:ERROR",
                "space": "nord",
                "target": "eea59a4b-ab88-4770-83f7-b491697626cf:Fat Molecular Cloud",
                "to_space": "nord"
            },
            {
                "from_space": "requests",
                "rule": "produce",
                "source": "31fadb9c-0c6f-4948-b428-5c38896ce8bb:Small Painted Giant",
                "space": "requests",
                "target": "d56fc77e-376d-4ba1-8e31-a2ed80e667de:Super Dental Bag",
                "to_space": "requests"
            },
            {
                "from_space": "sushrut",
                "rule": "assign",
                "source": "3ed92518-d6da-4011-87bf-4b65115a08da:http://echo.jsontest.com/key1/value1/key2/value2",
                "space": "nord",
                "target": "ddae20ac-f803-4f0e-8e61-1002c81a2af4:Windy Elevated Dog",
                "to_space": "athena"
            },
            {
                "from_space": "sushrut",
                "rule": "assign",
                "source": "40142890-5bfc-44fa-922b-250c7a05fbe0:GET Results Here",
                "space": "nord",
                "target": "0a933c2a-d37b-4821-97d2-792f6117079a:Prime Flooded Building",
                "to_space": "athena"
            },
            {
                "from_space": "kamal",
                "rule": "trueflow",
                "source": "5a60f21c-aba2-4025-9c56-b827252e2627:Mild Sanded Sound",
                "space": "kamal",
                "target": "1ae4bea6-b333-47fb-a15a-f0189eeca9af:ERROR",
                "to_space": "sushrut"
            },
            {
                "from_space": "kamal",
                "rule": "falseflow",
                "source": "5a60f21c-aba2-4025-9c56-b827252e2627:Mild Sanded Sound",
                "space": "kamal",
                "target": "f685a5b9-ed7e-444b-8c25-a9ca9857e714:OK",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "c60e981c-b5e2-4c92-90b6-a5fdd19a2d9c:200",
                "space": "nord",
                "target": "5a60f21c-aba2-4025-9c56-b827252e2627:Mild Sanded Sound",
                "to_space": "kamal"
            },
            {
                "delegate": {
                    "direction": "as_src",
                    "rule_type": "assign",
                    "uid": "produce_text_content"
                },
                "from_space": "requests",
                "rule": "assign",
                "source": "d56fc77e-376d-4ba1-8e31-a2ed80e667de:Super Dental Bag",
                "space": "nord",
                "target": "0a933c2a-d37b-4821-97d2-792f6117079a:Prime Flooded Building",
                "to_space": "athena"
            },
            {
                "delegate": {
                    "direction": "as_src",
                    "rule_type": "assign",
                    "uid": "produce_json_content"
                },
                "from_space": "requests",
                "rule": "assign",
                "source": "d56fc77e-376d-4ba1-8e31-a2ed80e667de:Super Dental Bag",
                "space": "nord",
                "target": "210aef4d-6e1e-47d8-b79c-4a1ec6a57cd9:Windy Sublimated Giant",
                "to_space": "sushrut"
            },
            {
                "delegate": {
                    "direction": "as_src",
                    "rule_type": "pass",
                    "uid": "produce_status"
                },
                "from_space": "requests",
                "rule": "pass",
                "source": "d56fc77e-376d-4ba1-8e31-a2ed80e667de:Super Dental Bag",
                "space": "nord",
                "target": "5a60f21c-aba2-4025-9c56-b827252e2627:Mild Sanded Sound",
                "to_space": "kamal"
            },
            {
                "delegate": {
                    "direction": "as_src",
                    "rule_type": "pass",
                    "uid": "on_value_0"
                },
                "from_space": "athena",
                "rule": "pass",
                "source": "ddae20ac-f803-4f0e-8e61-1002c81a2af4:Windy Elevated Dog",
                "space": "nord",
                "target": "31fadb9c-0c6f-4948-b428-5c38896ce8bb:Small Painted Giant",
                "to_space": "requests"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "f685a5b9-ed7e-444b-8c25-a9ca9857e714:OK",
                "space": "nord",
                "target": "40f977d3-97f5-413c-962c-5366fcf95235:Sharp Elevated Mountain",
                "to_space": "nord"
            }
        ],
        "nodes": [
            {
                "id": "0a933c2a-d37b-4821-97d2-792f6117079a",
                "name": "Prime Flooded Building",
                "position": [
                    20.303970336914062,
                    -0.3036956787109375,
                    -1.269256591796875
                ],
                "space": "athena",
                "type": "text"
            },
            {
                "id": "1ae4bea6-b333-47fb-a15a-f0189eeca9af",
                "name": "ERROR",
                "position": [
                    21.842355728149414,
                    1.0221633911132812,
                    7.556910037994385
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "ERROR"
            },
            {
                "contents": {},
                "id": "210aef4d-6e1e-47d8-b79c-4a1ec6a57cd9",
                "name": "Windy Sublimated Giant",
                "position": [
                    13.521505355834961,
                    -0.6320266723632812,
                    -5.093213081359863
                ],
                "space": "sushrut",
                "type": "dataobject"
            },
            {
                "id": "31fadb9c-0c6f-4948-b428-5c38896ce8bb",
                "name": "Small Painted Giant",
                "position": [
                    -0.9938726425170898,
                    1.6207275390625,
                    -1.4409186840057373
                ],
                "space": "requests",
                "type": "get"
            },
            {
                "id": "3ed92518-d6da-4011-87bf-4b65115a08da",
                "name": "http://echo.jsontest.com/key1/value1/key2/value2",
                "position": [
                    -1.091538906097412,
                    -0.08916091918945312,
                    3.7219674587249756
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "http://echo.jsontest.com/key1/value1/key2/value2"
            },
            {
                "id": "40142890-5bfc-44fa-922b-250c7a05fbe0",
                "name": "GET Results Here",
                "position": [
                    20.261550903320312,
                    -0.865386962890625,
                    -5.009514808654785
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "GET Results Here"
            },
            {
                "id": "40f977d3-97f5-413c-962c-5366fcf95235",
                "name": "Sharp Elevated Mountain",
                "position": [
                    27.959728240966797,
                    0.9847640991210938,
                    1.442509651184082
                ],
                "space": "nord",
                "type": "builtin"
            },
            {
                "id": "5a60f21c-aba2-4025-9c56-b827252e2627",
                "name": "Mild Sanded Sound",
                "position": [
                    12.545660018920898,
                    -0.27335357666015625,
                    3.302077054977417
                ],
                "space": "kamal",
                "type": "notequal"
            },
            {
                "id": "c60e981c-b5e2-4c92-90b6-a5fdd19a2d9c",
                "name": "200",
                "position": [
                    7.984159469604492,
                    -0.4226264953613281,
                    6.958740234375
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "200"
            },
            {
                "id": "d56fc77e-376d-4ba1-8e31-a2ed80e667de",
                "name": "Super Dental Bag",
                "position": [
                    7.9636101722717285,
                    -0.7294044494628906,
                    -1.4387248754501343
                ],
                "space": "requests",
                "type": "response"
            },
            {
                "id": "ddae20ac-f803-4f0e-8e61-1002c81a2af4",
                "name": "Windy Elevated Dog",
                "position": [
                    2.8577702045440674,
                    -0.04376220703125,
                    3.952845573425293
                ],
                "space": "athena",
                "type": "text"
            },
            {
                "id": "eea59a4b-ab88-4770-83f7-b491697626cf",
                "name": "Fat Molecular Cloud",
                "position": [
                    27.807701110839844,
                    0.9381027221679688,
                    7.524720191955566
                ],
                "space": "nord",
                "type": "builtin"
            },
            {
                "id": "f685a5b9-ed7e-444b-8c25-a9ca9857e714",
                "name": "OK",
                "position": [
                    21.691862106323242,
                    1.2930908203125,
                    1.4691479206085205
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "OK"
            }
        ]
    },
    "name": "Friendly Nordic Sue"
}